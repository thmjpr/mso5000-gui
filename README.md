# MSO5000 GUI


## Synopsis

Graphical interface for bode plotting and frequency generation on MSO5000 osciloscope.
SCPI control via TCPIP/USB/etc.


## Setup
- (Windows) Using pycharm load as a project and install the various modules: pyqt5, pyvisa, matplotlib, etc.
- Run "mso5000_bode_ui.py" for Bode plotting
- Run "mso5000_gen_ui.py" for function generator


## Issues
- Many


## Credits
- timber23 for source here: https://www.eevblog.com/forum/testgear/new-rigol-16-bit-function-generators-dg800900-series/msg2425551/#msg2425551

## License

MIT license where applicable.  