import time
import matplotlib.pyplot as plot
from mso5000 import MSO5000


class BodePlotting(object):
    scope = None

    scales = [0.002, 0.005, 0.010, 0.020, 0.050, 0.100, 0.200, 0.500, 1.000, 2.000, 5.000, 10.000]
    timebases = [0.000000001, 1]

    #Frequency
    f_start = 1000
    f_stop = 4000
    f_steps = 4  #number of steps

    amplitude = 1.000
    v_scale_ch_in = 1.000  #start at 1V scale
    v_scale_ch_out = 1.000  #start at 1V scale

    timebase = (1 / f_start) / 2  #show a few cycles on screen
    #need timebase to only select available ones...
    delay_ms = 500


    #specify the type, to allow autocomplete
    def __init__(self, scoperef: MSO5000):
        try:
            self.scope = scoperef
            test = self.scope.querying()
        except ValueError:
            pass

    def setupChannel(self, channel=1):
        self.scope.write(':channel1:display on')
        self.scope.write(':channel1:coupling AC')
        self.scope.write(':channel1:offset 0')
        self.scope.write(':channel1:scale 1.000')  #fixed for now, could adapt automatically. Fine vertical scale could be used to enhance resolution
        self.scope.write(':channel1:probe 1')  #1x probe could select it

    def setupTrigger(self, channel=1):
        self.scope.write(':trigger:mode edge')
        self.scope.write(':trigger:sweep normal')  #wait for trigger
        self.scope.write(':trigger:edge:source channel1')
        self.scope.write(':trigger:edge:slope pos')
        self.scope.write(':trigger:edge:level 0')
        #trigger location setting?

        #Setup time base
        self.scope.write(':timebase:mode main')  #YT mode

        #Run mode
        self.scope.write(':run')

        #disable generator
        #self.scope.write(':source1:output1 0')
        print(self.scope.getOutputState(channel))

        #Set generator output 1 voltage/frequency sine wave
        self.scope.write(':source1:volt 1')
        self.scope.write(':source1:freq 1000')
        self.scope.write(':source1:function sin')
        self.scope.write(':source1:voltage:offset 0')

        #need to disable sweep somehow?
        self.scope.write(':source1:')

        #enable generator output
        self.scope.setOutput(True, 1)

        #normal or precision measurement
        self.scope.write(':measure:mode normal')
        self.scope.write(':measure:source channel1')
        self.scope.write(':measure:clear all')  #clear all measurements on screen, unsure if necessary

        #Memory depth
        self.scope.write(':acq:type aver')
        self.scope.write(':acq:averages 128')
        self.scope.write(':acq:mdepth 100k')        #faster update

        print('Settings: ' + self.scope.query(':source1:apply?'))

        #setup phase measurement channels (A->B)
        self.scope.setupMeasPhase()


    #Probably scope autoscale would work too..
    def autoScale(self, scale=1.000, rounds=4, gen_channel=1, channel=1):
        #autorange the initial voltage scale to show waveform
        if gen_channel > 0:
            self.scope.setFrequency(self.f_start, gen_channel)      #Set starting F
        self.scope.setTimebase((1/self.f_start) / 2)            #See few

        for step in range(1, rounds):
            self.scope.setChannelScale(scale, channel)
            self.scope.displayClear()       # would this speed up the averaging of a new waveform?
            time.sleep(2.5)
            amp = self.scope.getMeasAmplitude(channel)

            # print out values as we record them
            print('freq: ' + str(self.f_start) + ' amp: ' + str(amp) + ' vscale: ' + str(scale * 0.5 / 10))

            #Assuming trace is not offset from 0V, scale until big enough
            if float(amp) < (scale * 3.5):  # We should be at least 6? divisions amplitude
                index = self.scales.index(scale)
                if index > 0:  # if we aren't at the lowest/highest scale (0 = first, -1 = last)
                    scale = self.scales[self.scales.index(scale) - 1]
                    #scale = 0.5
                else:
                    break  # stop trying since we are at limit
            else:
                break


    #If any channel is 0 it should be disabled
    #Valid gen_channel: 1,2
    #Valid io: 1,2,3,4
    def runBode(self, autoscale=1, gen_channel=1, channel_in=1, channel_out=2):
        amplitudes_in = []
        amplitudes_out = []
        frequencies = []
        phases = []

        frequency_step = (self.f_stop - self.f_start) / (self.f_steps - 1)  #calculate distance to step

        self.setupChannel()
        self.setupTrigger()

        timebase = (1 / self.f_start) / 2  #show a few cycles on screen

        #could also use fine control
        if autoscale:
            self.autoScale(self.v_scale_ch_in, 4, gen_channel, channel_in)
            self.autoScale(self.v_scale_ch_in, 4, gen_channel, channel_out)

        if gen_channel == 0:
            #this means no source is given, so we could measure the frequency on the screen instead of assuming it
            pass

        # Main measurement loop
        for step in range(1, self.f_steps + 1):
            cur_frequency = self.f_start + (frequency_step * (step - 1))
            timebase = (1 / cur_frequency) / 2
            self.scope.setTimebase(timebase)            #timebase is not based on actual steps yet
            self.scope.setFrequency(cur_frequency)
            self.scope.displayClear()
            time.sleep(self.delay_ms/1000)      #sleep x seconds

            current_amp_in = self.scope.getMeasAmplitude(channel_in)
            current_amp_out = self.scope.getMeasAmplitude(channel_out)
            current_phase = self.scope.getMeasPhase(channel_in, channel_out)
            #phase reads 1e38, needs to retry again or something
            if current_phase > 360:
                current_phase = -1

            amplitudes_in.append(current_amp_in)
            amplitudes_out.append(current_amp_out)
            phases.append(current_phase)
            frequencies.append(cur_frequency)       #this is the set frequency, not measured

            #print out values as we record them
            print('freq: ' + str(cur_frequency) + ' amp: ' + str(current_amp_in) +
                ' phase: ' + str(current_phase) +
                ' vscale: ' + str(self.v_scale_ch_in * 0.5 / 10) +
                ' timebase: ' + "{:.9f}".format(timebase))

            #some way to update progress bar here (may need threads?)
            #self.progressBar.value = step/(self.f_steps + 1)

        #if amplitude < scale * 0.5, increase scale? as it drops off maybe, but could go up too

        #close connection somewhere?
        self.scope.setOutput(False, gen_channel)        #turn off generator

        print(str(amplitudes_in))
        print(str(frequencies))

        plot.subplot(211)
        plot.plot(frequencies, amplitudes_in, '-', color='blue', lw=2)
        if channel_out != 0:
            plot.plot(frequencies, amplitudes_out, '-', color='green', lw=2)
        #if start frequency low or high change
        plot.xlabel('Frequency (Hz)')
        if True:
            plot.ylabel('Amplitude (V)')
        else:
            plot.ylabel('Amplitude (dB)')
        plot.yscale('linear')
        plot.title('Frequency response')
        plot.grid(True)

        plot.subplot(212)
        plot.plot(frequencies, phases, '-', lw=2)
        plot.xlabel('Frequency (Hz)')
        plot.ylabel('Phase (deg)')
        plot.ylim(-180, 180)           #0 to 180 degrees for now
        plot.yscale('linear')
        plot.title('Phase response')
        plot.grid(True)

        plot.tight_layout()
        plot.show()
        pass
        #some way to return frequencies and amplitudes?
        self.cleanup()

    def runBodeExternal(self, channel_in=1):
        amplitudes_in = []
        frequencies = []

        timebase = (1 / self.f_start) / 2  #show a few cycles on screen
        self.scope.setChannelScale(0.100, 1)

        # Main measurement loop
        for step in range(1, self.f_steps + 1):
            #self.scope.setTimebase(timebase)            #timebase is not based on actual steps yet
            self.scope.displayClear()
            time.sleep(self.delay_ms/1000)      #sleep x seconds

            current_amp_in = self.scope.getMeasAmplitude(channel_in)
            current_freq_in = self.scope.getMeasFrequency(channel_in)

            amplitudes_in.append(current_amp_in)        #measured amplitude
            frequencies.append(current_freq_in)         #measured frequency

            #print out values as we record them
            print('freq: ' + str(current_freq_in) + ' amp: ' + str(current_amp_in) +
                ' vscale: ' + str(self.v_scale_ch_in * 0.5 / 10) +
                ' timebase: ' + "{:.9f}".format(timebase))

        print(str(amplitudes_in))
        print(str(frequencies))

        plot.plot(frequencies, amplitudes_in, 'b.', color='blue')
        #if start frequency low or high change
        plot.xlabel('Frequency (Hz)')
        if True:
            plot.ylabel('Amplitude (V)')
        else:
            plot.ylabel('Amplitude (dB)')
        plot.yscale('linear')
        plot.title('Frequency response')
        plot.grid(True)

        plot.tight_layout()
        plot.show()

    def cleanup(self):
        self.scope.write(':acq:mdepth auto')  #can restore settings instead


        ##FF remove 0 points in the plot
