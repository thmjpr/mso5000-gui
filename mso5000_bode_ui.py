import sys
from PyQt5 import QtWidgets, uic
from PyQt5.QtGui import QPixmap
import time
from PyQt5.QtWidgets import QFileDialog
from threading import Thread

from mso5000 import MSO5000
from bode_plotting import BodePlotting

qtCreatorFile = "Bode_GUI.ui" # Enter file here.
Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)


class MyApp(QtWidgets.QMainWindow, Ui_MainWindow):
    def toggle_output_channel_1(self):
        if self.inst.readOutputState(1):        #seems like it returns 1 or 0 not off or on
            self.inst.writing(':source1:output OFF')
            self.buttonOutput1.setText("Output ON")
        else:
            self.inst.writing(':source1:output ON')
            self.buttonOutput1.setText("Output OFF")

    def liveUpdate(self):
        pass

    def toggle_output_channel_2(self):
        if self.inst.readOutputState(2):
            self.inst.writing(':source2:output OFF')
        else:
            self.inst.writing(':source2:output ON')
    
    def tryConnect(self):
        myDevice = self.boxDevices.currentText()
        self.inst.conn(myDevice)
        self.liveUpdate()
        self.statusBar().showMessage('Connected to: '+myDevice)
        self.getLiveview()
    
    def setWaveform(self):
        self.inst.writing(':source1:function '+self.boxWaveform.currentText())
        self.liveUpdate()
    
    def setWaveform2(self):
        self.inst.writing(':source2:function '+self.boxWaveform2.currentText())
        self.liveUpdate()
        
    def getLiveview(self):
        #png = self.inst.getPNG()
        #with open('tmp.png',"wb") as f:
        #    f.write(png)
        #mypix = QPixmap('tmp.png')
        #self.labelView.setPixmap(mypix)
        test = 0
        
    def toggleHighZChan1(self):
        Status = self.inst.querying(":OUTP1:IMP?")
        if (Status=="5.000000E+01"):
            self.inst.setImpedance(self.Impedance.HighZ, 1)
            self.inst.writing(":OUTP1:IMP INF")
        else:
            self.inst.writing(":OUTP1:LOAD 50")
        self.liveUpdate()
    
    def toggleHighZChan2(self):
        Status = self.inst.querying(":OUTP2:IMP?")
        if (Status=="5.000000E+01"):
            self.inst.writing(":OUTP2:IMP INF")
        else:
            self.inst.writing(":OUTP2:LOAD 50")
        self.liveUpdate()
    
    def takeScreenshot(self):
        fname = QFileDialog.getSaveFileName()#(self, 'Open file', '/home')
        png = self.inst.getPNG()
        with open(fname[0],"wb") as f:
            f.write(png)
    
    def addDevices(self):
        devs = self.inst.findDevs()
        self.boxDevices.clear()
        for device in devs:
            if ("USB" in device) | ("TCPIP" in device):
                self.boxDevices.addItem(device)
                
    def sendSCPI(self):
        SCPIcommand = self.textSCPI.text()
        if "?" in SCPIcommand:
            resp = self.inst.querying(SCPIcommand)
            self.textSCPI.setText(resp)
        else:
            self.inst.writing(SCPIcommand)

    #Set number of bode frequency steps
    def setBodeSteps(self):
        numBodeSteps = self.sliderSteps.value()
        self.lblNumSteps.setText(str(self.sliderSteps.value()))
        self.lblDuration.setText('Duration: ' + str(self.sliderSteps.value() * self.sliderDelay.value()/1000) + 's')
        self.calcStepFrequency()

    #Calculate what amount each frequency step is
    def calcStepFrequency(self):
        step = (int(self.textFrequencyStop.text()) - int(self.textFrequencyStart.text()))/(self.sliderSteps.value()-1)
        self.lblFrequencyStep.setText('Step: ' + str(step) + 'Hz')
        #need to add multiplier

    #Set number of bode frequency steps
    def setDelayTime(self):
        test = self.sliderDelay.value()
        self.lblNumDelay.setText(str(self.sliderDelay.value() ) + " ms")
        self.lblDuration.setText('Duration: ' + str(self.sliderSteps.value() * self.sliderDelay.value() / 1000) + 's')

    def buttonsLockedOut(self, state=True):
        if state:
            self.buttonStart.setEnabled(False)
            self.buttonStartExternal.setEnabled(False)
        else:
            self.buttonStart.setEnabled(True)
            self.buttonStartExternal.setEnabled(True)

    #Start an internally controlled bode plot (swept with generator)
    def startBode(self):
        self.buttonsLockedOut(True)
        bode = BodePlotting(self.inst)    #need to reference scope somehow?
        #bode = Thread(target=BodePlotting, args=self.inst)
        bode.f_start = int(self.textFrequencyStart.text()) * 1000 ** self.boxFreqMultiplicator.currentIndex()
        bode.f_stop = int(self.textFrequencyStop.text()) * 1000 ** self.boxFreqMultiplicator.currentIndex()
        bode.amplitude = float(self.textAmplitude.text())     #have to scale later..
        #if VRMS, vdB, etc.
        bode.f_steps = int(self.sliderSteps.value())     #cant directly get text value from lbl, rich text
        bode.delay_ms = int(self.sliderDelay.value())

        #pass autoscale, gen channel and input/output channels
        autoscale = self.chkAutoscale.isChecked()
        source = self.boxSource.currentIndex()
        inp = self.boxInputChannel.currentIndex()
        out = self.boxOutputChannel.currentIndex()
        bode.runBode(autoscale=autoscale, gen_channel=source, channel_in=inp, channel_out=out)
        self.buttonsLockedOut(False)

    #Start an externally modulated bode plot (swept from external source)
    def startBodeExternal(self):
        self.buttonsLockedOut(True)
        bode = BodePlotting(self.inst)  #need to reference scope somehow?
        bode.amplitude = float(self.textAmplitude.text())  #have to scale later..
        #if VRMS, vdB, etc.
        bode.f_steps = int(self.sliderSteps.value())
        bode.delay_ms = int(self.sliderDelay.value())
        autoscale = self.chkAutoscale.isChecked()
        gen_source = self.boxSource.currentIndex()
        inp = self.boxInputChannel.currentIndex()
        bode.runBodeExternal(channel_in=inp)
        self.buttonsLockedOut(False)

    def waitForTimeout(self):
        pass


    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)
        self.inst = MSO5000()

        self.buttonStartExternal.clicked.connect(self.startBodeExternal)

        self.buttonConnect.clicked.connect(self.tryConnect)
        self.buttonStart.clicked.connect(self.startBode)

        self.sliderSteps.valueChanged.connect(self.setBodeSteps)
        self.sliderDelay.valueChanged.connect(self.setDelayTime)

        #self.textFrequencyChannel1.returnPressed.connect(self.setFreqChan1)
        #self.textAmplitudeChannel1.returnPressed.connect(self.setAmpChannel1)

        self.boxWaveform.currentIndexChanged.connect(self.setWaveform)

        self.statusBar().showMessage('Not connected.')
        self.addDevices()
        self.tryConnect()
 
if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = MyApp()
    window.show()
    #sys.exit(app.exec_())
    app.exec_()
    del app