import sys
from PyQt5 import QtWidgets, uic
from PyQt5.QtGui import QPixmap
import time
from PyQt5.QtWidgets import QFileDialog
from mso5000 import MSO5000

qtCreatorFile = "Gen_GUI.ui" # Enter file here.
Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)


class MyApp(QtWidgets.QMainWindow, Ui_MainWindow):

    def liveUpdate(self):
        if(0): #self.checkLive.isChecked()):
            self.getLiveview()
            self.statusBar().showMessage('Wait 3 seconds for refresh.')
            time.sleep(1)
            self.statusBar().showMessage('Wait 2 seconds for refresh.')
            time.sleep(1)
            self.statusBar().showMessage('Wait 1 seconds for refresh.')
            time.sleep(1)
            self.getLiveview()
            self.statusBar().showMessage('Liveview updated.')
    
    def toggle_output_channel_1(self):
        if self.inst.readOutputState("1") == "1":        #seems like it returns 1 or 0 not off or on
            self.inst.setOutput(False)
        else:
            self.inst.setOutput(True)
        
    def toggle_output_channel_2(self):
        if self.inst.readOutputState("2") == "0":
            self.inst.setOutput(False, 2)
        else:
            self.inst.setOutput(True, 2)
    
    def tryConnect(self):
        myDevice = self.boxDevices.currentText()
        self.inst.conn(myDevice)
        self.liveUpdate()
        self.statusBar().showMessage('Connected to: '+myDevice)
        self.getLiveview()
    
    def setWaveform(self):
        self.inst.setFunction(self.boxwaveform.currentText(), 1)
        self.liveUpdate()
    
    def setWaveform2(self):
        self.inst.setFunction(self.boxwaveform.currentText(), 2)
        self.liveUpdate()
        
    def setFreqChan1(self):
        freq = int(self.textFrequencyChannel1.text()) * 1000**self.boxFreqMultiplicator.currentIndex()
        if freq <= 25000000:
            self.inst.setFrequency(str(freq))
            self.liveUpdate()
    
    def setFreqChan2(self):
        freq = int(self.textFrequencyChannel2.text())*1000**self.boxFreqMultiplicator2.currentIndex()
        if freq <= 25000000:
            self.inst.setFrequency(str(freq), 2)
            self.liveUpdate()
    
    def setAmpChannel1(self):
        amp = self.textAmplitudeChannel1.text()
        #self.inst.writing(':SOUR1:VOLT:UNIT '+self.boxUnitChannel1.currentText())
        self.inst.setVoltage(amp)
        self.liveUpdate()
    
    def setAmpChannel2(self):
        amp = self.textAmplitudeChannel2.text()
        #self.inst.writing(':SOUR2:VOLT:UNIT '+self.boxUnitChannel2.currentText())
        self.inst.setVoltage(amp, 2)
        self.liveUpdate()
        
    def getLiveview(self):
        #png = self.inst.getPNG()
        #with open('tmp.png',"wb") as f:
        #    f.write(png)
        #mypix = QPixmap('tmp.png')
        #self.labelView.setPixmap(mypix)
        test = 0
        
    def toggleHighZChan1(self):
        Status = self.inst.getImpedance(1)
        if (Status=="5.000000E+01"):
            self.inst.setImpedance(self.Impedance.HighZ, 1)
        else:
            self.inst.setImpedance(self.Impedance.FiftyOhm, 1)
        self.liveUpdate()
    
    def toggleHighZChan2(self):
        Status = self.inst.getImpedance(2)
        if (Status=="5.000000E+01"):
            self.inst.setImpedance(self.Impedance.HighZ, 2)
        else:
            self.inst.setImpedance(self.Impedance.FiftyOhm, 2)
        self.liveUpdate()
    
    def takeScreenshot(self):
        fname = QFileDialog.getSaveFileName()#(self, 'Open file', '/home')
        png = self.inst.getPNG()
        with open(fname[0],"wb") as f:
            f.write(png)
    
    def addDevices(self):
        devs = self.inst.findDevs()
        self.boxDevices.clear()
        for device in devs:
            if ("USB" in device) | ("TCPIP" in device):
                self.boxDevices.addItem(device)
                
    def sendSCPI(self):
        SCPIcommand = self.textSCPI.text()
        if "?" in SCPIcommand:
            resp = self.inst.querying(SCPIcommand)
            self.textSCPI.setText(resp)
        else:
            self.inst.writing(SCPIcommand)

    
    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)
        self.inst = MSO5000()
        self.buttonOutput1.clicked.connect(self.toggle_output_channel_1)
        self.buttonOutput2.clicked.connect(self.toggle_output_channel_2)
        self.buttonConnect.clicked.connect(self.tryConnect)
        self.buttonLive.clicked.connect(self.getLiveview)
        self.buttonRefresh.clicked.connect(self.addDevices)
        self.buttonToggleHighZ_Ch1.clicked.connect(self.toggleHighZChan1)
        self.buttonToggleHighZ_Ch2.clicked.connect(self.toggleHighZChan2)
        self.textFrequencyChannel1.returnPressed.connect(self.setFreqChan1)
        self.textFrequencyChannel2.returnPressed.connect(self.setFreqChan2)
        self.textAmplitudeChannel1.returnPressed.connect(self.setAmpChannel1)
        self.textAmplitudeChannel2.returnPressed.connect(self.setAmpChannel2)
        self.textSCPI.returnPressed.connect(self.sendSCPI)
        self.buttonSCPI.clicked.connect(self.sendSCPI)
        self.boxWaveform.currentIndexChanged.connect(self.setWaveform)
        self.boxWaveform2.currentIndexChanged.connect(self.setWaveform2)
        self.statusBar().showMessage('Not connected.')
        self.addDevices()
        self.buttonScreenshot.clicked.connect(self.takeScreenshot)
        self.tryConnect()
 
if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = MyApp()
    window.show()
    #sys.exit(app.exec_())
    app.exec_()
    del app